package com.tora;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> hashSet;

    public HashSetBasedRepository() {
        this.hashSet = new HashSet<>();
    }

    @Override
    public void add(T value) {
        hashSet.add(value);
    }

    @Override
    public boolean contains(T value) {
        return hashSet.contains(value);
    }

    @Override
    public void remove(T value) {
        hashSet.remove(value);
    }
}
