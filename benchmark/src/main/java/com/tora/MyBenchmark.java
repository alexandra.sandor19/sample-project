package com.tora;/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

import com.tora.ArrayListBasedRepository;
import com.tora.HashSetBasedRepository;
import com.tora.TreeSetBasedRepository;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class MyBenchmark {

    @State(Scope.Benchmark)
    public static class ArrayListState {
        ArrayListBasedRepository<String> set = new ArrayListBasedRepository();
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        HashSetBasedRepository<String> set = new HashSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        TreeSetBasedRepository<String> set = new TreeSetBasedRepository<>();
    }

    @Benchmark
    public void addToArray(ArrayListState state) {
        state.set.add("hello");
    }

    @Benchmark
    public void addToHashSet(HashSetState state) {
        state.set.add("to");
    }

    @Benchmark
    public void addToTreeSet(TreeSetState state) {
        state.set.add("you");
    }

    @Benchmark
    public boolean constainsInArray(ArrayListState state) {
        return state.set.contains("hello");
    }

    @Benchmark
    public boolean containsInHashSet(HashSetState state) {
        return state.set.contains("not");
    }

    @Benchmark
    public boolean containsTreeSet(TreeSetState state) {
        return state.set.contains("you");
    }

    @Benchmark
    public void removeFromArray(ArrayListState state) {
        state.set.remove("hello");
    }

    @Benchmark
    public void removeFromHashSet(HashSetState state) {
        state.set.remove("to");
    }

    @Benchmark
    public void removeFromTreeSet(TreeSetState state) {
        state.set.remove("you");
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
