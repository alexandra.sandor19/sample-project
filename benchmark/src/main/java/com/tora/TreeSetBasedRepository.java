package com.tora;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> treeSet;

    public TreeSetBasedRepository() {
        this.treeSet = new TreeSet<>();
    }

    @Override
    public void add(T value) {
        treeSet.add(value);
    }

    @Override
    public boolean contains(T value) {
        return treeSet.contains(value);
    }

    @Override
    public void remove(T value) {
        treeSet.remove(value);
    }
}
