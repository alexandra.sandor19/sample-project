package com.calculator;

import com.calculator.operator.Operator;
import com.calculator.validator.IValidator;
import com.calculator.validator.Validator;

import java.util.Scanner;

public class Calculator {
    private Number result;
    private final IValidator validator;

    public Calculator() {
        this.result = 0;
        this.validator = new Validator();
    }

    public void performOperation(Operator operator, String argument) throws Exception {
        Number number = validator.validateNumber(argument);
        this.result = operator.compute(result, number);
    }

    public void clearAll() {
        this.result = 0;
    }

    public String enterNumber(Scanner scanner) {
        System.out.println("Enter number:");
        return scanner.next();
    }

    public String enterCommand(Scanner scanner) {
        System.out.println("Select an operation");
        System.out.println(" --------------------------------------------------");
        System.out.println("|    +    |    -    |    *    |    /    |    CA    |");
        System.out.println(" --------------------------------------------------\n");
        return scanner.next();
    }

    public void runCalculator() throws Exception {
        Scanner scanner = new Scanner(System.in);
        String STOP_KEY = "end";

        while(true) {
            System.out.println("----------------------------------------------------");
            System.out.println("CURRENT VALUE OF CALCULATOR: " + this.result);
            System.out.println("To turn off, type: " + STOP_KEY);
            System.out.println("----------------------------------------------------");

            // if this is the first operation, we ask for a number input
            if (this.result.equals(0)) {
                String input = this.enterNumber(scanner);
                // if we want to turn off the calculator
                if (input.equalsIgnoreCase(STOP_KEY)) {
                    break;
                }
                this.result = this.validator.validateNumber(input);
            }
            String command = this.enterCommand(scanner);

            if (command.equalsIgnoreCase(STOP_KEY)) {
                break;
            }
            else
            if (command.equalsIgnoreCase("CA")) {
                this.clearAll();
            }
            else {
                Operator operator = this.validator.validateOperator(command);
                String numberInput = this.enterNumber(scanner);
                this.performOperation(operator, numberInput);
            }
        }
    }
}
