package com.calculator;

// Calculator implementation
// User inputs a number (integer or real); The input has to be validated then converted into their respective type
// They next choose an operation (+,-,/,*)
// The result is shown on the screen
// The user can delete the result and create a new operation (CA) or continue applying operations on the result

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        try {
            calculator.runCalculator();
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
