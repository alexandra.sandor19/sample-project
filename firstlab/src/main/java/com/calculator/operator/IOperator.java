package com.calculator.operator;

public interface IOperator {
    Number compute(Number first, Number second) throws Exception;
}
