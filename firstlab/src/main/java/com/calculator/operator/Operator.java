package com.calculator.operator;

/*
    For each operation, the values are computed as floats.
    If the result of the computation has fractional part 0, it will be cast and returned as an integer;
    Otherwise, it is returned as a float.
 */
public enum Operator implements IOperator {
    PLUS {
        @Override
        public Number compute(Number first, Number second) {
            float sum = first.floatValue() + second.floatValue();
            if (sum % 1 == 0) {
                return (int) sum;
            }
            else {
                return sum;
            }
        }
    },
    MINUS {
        @Override
        public Number compute(Number first, Number second) {
            float rest = first.floatValue() - second.floatValue();
            if (rest % 1 == 0) {
                return (int) rest;
            }
            else {
                return rest;
            }
        }
    },
    MULTIPLY {
        @Override
        public Number compute(Number first, Number second) {
            float product = first.floatValue() * second.floatValue();
            if (product % 1 == 0) {
                return (int) product;
            }
            else {
                return product;
            }
        }
    },
    DIVIDE {
        @Override
        public Number compute(Number first, Number second) {
            if (second.equals(0)) {
                throw new ArithmeticException("Division by zero is forbidden!");
            }
            float quotient = first.floatValue() / second.floatValue();
            if (quotient % 1 == 0) {
                return (int) quotient;
            }
            else {
                return quotient;
            }
        }
    }
}
