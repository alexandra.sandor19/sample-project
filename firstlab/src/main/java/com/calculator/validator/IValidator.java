package com.calculator.validator;

import com.calculator.operator.Operator;

public interface IValidator {
    /*
    Checks if the received input is a number.
    If yes, the input is parsed and returned as a float.
    If not, an exception is thrown.
     */
    Number validateNumber(String input) throws Exception;
    /*
    Checks if the received input is an arithmetic operator.
    If yes, the operator is returned.
    If not, an exception is thrown.
     */
    Operator validateOperator(String input) throws Exception;
}
