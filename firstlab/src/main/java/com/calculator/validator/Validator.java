package com.calculator.validator;

import com.calculator.exceptions.InvalidInputException;
import com.calculator.operator.Operator;

public class Validator implements IValidator{
    @Override
    public Number validateNumber(String input) throws Exception {
        try {
            return Float.parseFloat(input);
        }
        catch (Exception ex) {
            // if the input couldn't be parsed as a number, exception is thrown
            throw new InvalidInputException("Input for number is invalid!");
        }
    }

    @Override
    public Operator validateOperator(String input) throws Exception {
        if (input.equals("+")) {
            return Operator.PLUS;
        }
        if (input.equals("-")) {
            return Operator.MINUS;
        }
        if (input.equals("*")) {
            return Operator.MULTIPLY;
        }
        if (input.equals("/")) {
            return Operator.DIVIDE;
        }
        throw new InvalidInputException("Invalid command!\nChoose one of the following operators(+, -, *, /) or CA(clear all).\n");
    }

}
