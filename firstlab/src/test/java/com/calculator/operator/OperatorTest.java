package com.calculator.operator;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OperatorTest {

    @Test
    public void addIntegers() throws Exception {
        var toAdd = Operator.PLUS;
        assertEquals(4, toAdd.compute(2, 2));
        assertEquals(-12, toAdd.compute(-3, -9));
        assertNotEquals(-10, toAdd.compute(-8, 12));
    }

    @Test
    public void addIntegerAndFloats() throws Exception {
        var toAdd = Operator.PLUS;
        assertEquals((float)37.91, toAdd.compute(30, 7.91));
        assertEquals((float)-23.891, toAdd.compute(-25.891, 2));
        assertNotEquals((float)10.2, toAdd.compute(7.3, 2));
    }

    @Test
    public void addFloats() throws Exception {
        var toAdd = Operator.PLUS;
        assertEquals((float)-5.76, toAdd.compute(-2.1, -3.66));
        assertEquals(25, toAdd.compute(21.3, 3.7));
        assertNotEquals((float)10.2, toAdd.compute(7.3, 2.1));
    }

    @Test
    public void subtractIntegers() throws Exception {
        var toSub = Operator.MINUS;
        assertEquals(7, toSub.compute(10, 3));
        assertEquals(-54, toSub.compute(-45, 9));
        assertNotEquals(-11, toSub.compute(-102, 34));
    }

    @Test
    public void subtractIntegerAndFloats() throws Exception {
        var toSub = Operator.MINUS;
        assertEquals((float)-65.25, toSub.compute(-61.25, 4));
        assertNotEquals((float)69.4, toSub.compute(76.3, 7));
    }

    @Test
    public void subtractFloats() throws Exception {
        var toSub = Operator.MINUS;
        assertEquals((float)-2.1, toSub.compute(4.1, 6.2));
        assertEquals((float)1.5, toSub.compute(-2.1, -3.6));
        assertNotEquals((float)10.2, toSub.compute(7.3, 2.1));
    }

}