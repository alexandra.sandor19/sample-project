package com.lab;
import java.io.*;
import java.math.RoundingMode;
import java.util.*;
import java.math.BigDecimal;
import java.util.stream.Collectors;

public class BigDecimalMain {
    private List<BigDecimal> listOfBD;
    private int listLength;

    public BigDecimalMain(int size) {
        this.listOfBD = new ArrayList<>();
        this.listLength = size;
        createBDList(size);
    }

    public void createBDList(int listLength) {
        for (int i = 0; i < listLength; i++) {
            listOfBD.add(new BigDecimal(i));
        }
    }

    public BigDecimal computeSum() {
        return listOfBD.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal computeAverage() {
        return computeSum().divide(new BigDecimal(listLength), 3, RoundingMode.CEILING);
    }

    public List<BigDecimal> getTopNumbers() {
        int top = listLength / 10;
        return listOfBD.stream().sorted(Comparator.reverseOrder()).limit(top).collect(Collectors.toList());
    }

    public void serialize(String filename, List<BigDecimal> listOfBd) {
        try
        {
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);
            listOfBd.stream().forEach(el -> {
                try {
                    out.writeObject(el);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            out.close();
            file.close();
        }
        catch(IOException ex)
        {
            System.out.println("IOException is caught");
        }
    }

    public void deserialize(String filename) {
        try
        {
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            var listBD = new ArrayList<BigDecimal>();
            listBD = (ArrayList<BigDecimal>) in.readObject();

            for(int i=0; i<listBD.size(); i++){
                System.out.println(listBD.get(i));
            }

            in.close();
            file.close();
        }

        catch(IOException ex)
        {
            System.out.println("IOException is caught");
        }

        catch(ClassNotFoundException ex)
        {
            System.out.println("ClassNotFoundException is caught");
        }
    }


    public static void main(String[] args) {
        var bd = new BigDecimalMain(100);
        System.out.println(bd.listOfBD);
        System.out.println(bd.computeSum());
        System.out.println(bd.computeAverage());
        System.out.println(bd.getTopNumbers());
        bd.serialize("file.bin", bd.listOfBD);
        // bd.deserialize("file.bin");
    }
}
