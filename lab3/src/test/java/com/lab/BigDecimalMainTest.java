package com.lab;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class BigDecimalMainTest {

    private BigDecimalMain bigDecimals;
    private double sum;
    private double average;
    private List<Integer> topTenPercent;
    private final int SIZE = 1000;

    @BeforeEach
    void setUp() throws Exception {
        bigDecimals = new BigDecimalMain(SIZE);
        sum = 0;
        for (int i = 0; i < SIZE; i++) {
            sum += i;
        }
        average = sum / SIZE;
        int numbers = SIZE / 10;
        topTenPercent = new ArrayList<>();
        for (int i = SIZE-1; i >= SIZE-numbers; i--) {
            topTenPercent.add(i);
        }
    }

    @Test
    void computeSum() {
        assertEquals(bigDecimals.computeSum(), new BigDecimal(sum));
    }

    @Test
    void computeAverage() {
        assertEquals(bigDecimals.computeAverage(), new BigDecimal(average).setScale(3));
    }

    @Test
    void getTopNumbers() {
        assertEquals(bigDecimals.getTopNumbers(), topTenPercent.stream().map(BigDecimal::new).collect(Collectors.toList()));
    }
}